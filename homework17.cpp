#include <iostream>
#include <math.h>
#include <vector>

class Vector
{
private:
	double x;
	double y;
	double z;
public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Show()
	{
		std::cout << "\n" << x << " " << y << " " << z;
	}
	void Show2()
	{
		double length;
		length = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
		std::cout << length << "\n";
	}
};

int main()
{
	Vector v(2, 2, 2);
	v.Show();
	Vector s(3, 4, 5);
	s.Show2();
	return 0;
}